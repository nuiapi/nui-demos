#include "customcontroller.h"
#include <sstream>

using namespace demos;

void CustomController::OnResize(
    nui::event::ResizeEventArgs *args)
{
    nui::Label *label = dynamic_cast<nui::Label *>(args->Sender()->App()->Find(L"label"));

    if (label != nullptr)
    {
        std::wstringstream ss;

        ss << L"OnResize : " << args->Width() << L" - " << args->Height();
        label->Text(ss.str());
    }
}
