
#include <flui-nui.h>
#include <nui.h>
#include <sstream>

extern const wchar_t MainWidget[] = L"MainWidget";
extern const wchar_t label[] = L"label";

#include "customcontroller.h"

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    using namespace nui::event;

    demos::CustomController controller;

    try
    {
        return FluiNui()
            .MainWidget<MainWidget, demos::CustomController>(
                L"nui Controller Demo",
                [&](auto builder) {
                    builder
                        .FixedWidth(640)
                        .FixedHeight(480)
                        .OnResize((ResizeEventHandlerFn)&demos::CustomController::OnResize);

                    builder
                        .Label<label>(L"Try resizing the window");
                })
            .Run();
    }
    catch (const std::exception &ex)
    {
        std::string cs(ex.what());
        std::wstring ws;
        std::copy(cs.begin(), cs.end(), std::back_inserter(ws));

        nui::Application::MessagePopUp(ws);

        return 0;
    }
}
