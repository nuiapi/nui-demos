#ifndef CUSTOMCONTROLLER_H
#define CUSTOMCONTROLLER_H

#include <nui.h>

namespace demos
{

    class CustomController : public nui::Controller
    {
    public:
        void OnResize(
            nui::event::ResizeEventArgs *args);
    };

} // namespace demos

#endif // CUSTOMCONTROLLER_H
