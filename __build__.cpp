
#include <cppbuild.hpp>

int main(int argc, char* argv[])
{
    cppbuild::init(argc, argv);
    
    cppbuild::Target controller("controller");
    
    controller.folder("controller", {
        "customcontroller.cpp",
        "program.cpp",
    });

    controller.libraries({
        "nui",
        "opengl32",
        "gdi32",
    });

    controller.includeDirs({
        "%WIN_CPP_PREFIX_PATH%\\include"
    });

    controller.libraryDirs({
        "%WIN_CPP_PREFIX_PATH%\\lib"
    });



    cppbuild::Target mainwidget("mainwidget");
    
    mainwidget.folder("mainwidget", {
        "program.cpp",
    });

    mainwidget.libraries({
        "nui",
        "opengl32",
        "gdi32",
    });

    mainwidget.includeDirs({
        "%WIN_CPP_PREFIX_PATH%\\include"
    });

    mainwidget.libraryDirs({
        "%WIN_CPP_PREFIX_PATH%\\lib"
    });



    cppbuild::Target glwidget("glwidget");
    
    glwidget.folder("glwidget", {
        "customcontroller.cpp",
        "program.cpp",
    });

    glwidget.libraries({
        "nui",
        "opengl32",
        "gdi32",
    });

    glwidget.includeDirs({
        "glwidget",
        "%WIN_CPP_PREFIX_PATH%\\include"
    });

    glwidget.libraryDirs({
        "%WIN_CPP_PREFIX_PATH%\\lib"
    });

    return 0;
}
