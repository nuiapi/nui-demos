
#include <flui-nui.h>
#include <nui.h>
#include <sstream>

extern const wchar_t MainWidget[] = L"MainWidget";
extern const wchar_t OpenGl[] = L"OpenGl";

#include "customcontroller.h"

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    using namespace nui::event;

    try
    {
        return FluiNui()
            .MainWidget<MainWidget, demos::CustomController>(
                L"nui Gl Widget Demo",
                [&](ContainerBuilder &builder) {
                    builder
                        .FixedWidth(400)
                        .FixedHeight(300);

                    builder
                        .GlLatest(
                            L"OpenGl",
                            [&](WidgetBuilder &builder) {
                                builder
                                    .FireEvent((EventHandlerFn)&demos::CustomController::OnInit);
                            });
                })
            .OnIdle((IdleEventHandlerFn)&demos::CustomController::OnIdle)
            .OnExit((ExitEventHandlerFn)&demos::CustomController::OnExit)
            .Run();
    }
    catch (const std::exception &ex)
    {
        std::string cs(ex.what());
        std::wstring ws;
        std::copy(cs.begin(), cs.end(), std::back_inserter(ws));

        nui::Application::MessagePopUp(ws);

        return 0;
    }
}
