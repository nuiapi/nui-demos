#include "customcontroller.h"
#include <glad/glad.h>
#include <iostream>
#include <sstream>

using namespace demos;

CustomController::CustomController() = default;

void CustomController::OnInit(
    nui::event::EventArgs *args)
{
    this->gl = (nui::GlWidget *)args->Sender();

    glClearColor(0.3f, 0.7f, 1.0f, 1.0f);

    std::cout << "GL_VERSION                  : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GL_SHADING_LANGUAGE_VERSION : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GL_RENDERER                 : " << glGetString(GL_RENDERER) << std::endl;
    std::cout << "GL_VENDOR                   : " << glGetString(GL_VENDOR) << std::endl;
}

void CustomController::OnIdle(
    nui::event::IdleEventArgs *args)
{
    if (this->gl != nullptr)
    {
        this->gl->BeginFrame();
        glClear(GL_COLOR_BUFFER_BIT);
        this->gl->EndFrame();
    }
}

void CustomController::OnResize(
    nui::event::ResizeEventArgs *args)
{
    if (this->gl != nullptr)
    {
        this->gl->BeginFrame();
        glViewport(0, 0, args->Width(), args->Height());
        this->gl->EndFrame();
    }
}

void CustomController::OnExit(
    nui::event::ExitEventArgs *args)
{}
