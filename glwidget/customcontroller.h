#ifndef CUSTOMCONTROLLER_H
#define CUSTOMCONTROLLER_H

#include <nui.h>

namespace demos
{

    class CustomController : public nui::Controller
    {
    public:
        CustomController();

        void OnInit(nui::event::EventArgs *args);
        void OnIdle(nui::event::IdleEventArgs *args);
        void OnResize(nui::event::ResizeEventArgs *args);
        void OnExit(nui::event::ExitEventArgs *args);

    private:
        nui::GlWidget *gl = nullptr;
    };

} // namespace demos

#endif // CUSTOMCONTROLLER_H
