
#include <flui-nui.h>
#include <nui.h>
#include <sstream>

extern const wchar_t MainWidget[] = L"MainWidget";

namespace demos
{

    class CustomController : public nui::Controller
    {
    };

} // namespace demos

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    try
    {
        return FluiNui()
            .MainWidget<MainWidget, demos::CustomController>(
                L"nui MainWidget Demo",
                [](auto builder) {
                    builder
                        .FixedWidth(300)
                        .FixedHeight(200);
                })
            .Run();
    }
    catch (const std::exception &ex)
    {
        std::string cs(ex.what());
        std::wstring ws;
        std::copy(cs.begin(), cs.end(), std::back_inserter(ws));

        nui::Application::MessagePopUp(ws);

        return 0;
    }
}
