cmake_minimum_required(VERSION 3.13)

add_executable(nui-mainwidget
    program.cpp
)

target_compile_definitions(nui-mainwidget
    PUBLIC
        UNICODE
        _UNICODE
)

target_link_libraries(nui-mainwidget
    nui
)
